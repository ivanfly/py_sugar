import pandas
import matplotlib.pyplot as plt
import seaborn
#開啟CSV
data = pandas.read_csv('Titanic_train.csv')
#過濾生存者
filt = (data['Survived'] == 1)
data = data.loc[filt]
print(data)

#產生直條圖+常態分析
seaborn.set_style("whitegrid")
res=seaborn.distplot(data['Age'])
plt.show()