class Student(object):  # l開啟一個類別,代表之後的大家都可以取用這個類別的設定,達到避免重複設定函式的目的

    def __init__(self, name, age):  # def __init__(self):宣告時會自動執行的函式
        self.name = name
        self.age = age
        
    def study(self, course_name):
       print("%s正在學習%s" % (self.name, course_name))
    
    def watch_movie(self):
        if self.age < 18:
            print('%s只能觀看<熊出没>.' % self.name)
        else:
            print('%s正在觀看島國愛情大電影.' % self.name)


a = Student('IVAN', 17)  # name='poem',age=20
a.watch_movie()
a.study('小黃書')
