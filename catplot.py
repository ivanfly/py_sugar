import pandas
import matplotlib.pyplot as plt
import seaborn
#開啟CSV
data = pandas.read_csv('Titanic_train.csv')
print(data)

#
g = seaborn.catplot(
    data=data, kind="count",
    x="Sex", hue="Survived"
)
g.despine(left=True)
plt.show()