# This Python file uses the following encoding: utf-8

import logging as log
import mysql.connector
import json
import ckip
import inltk
import schedule
import time
import re
FORMAT = '%(asctime)s %(levelname)s: %(message)s'
log.basicConfig(level=log.DEBUG, filename='crawler.log', filemode='w', format=FORMAT)
def merge(tags1, tags2):
    outs = []
    outs += tags1
    if tags2 is not None:
        for tag2 in tags2:
            if tag2 not in outs:
                outs.append(tag2)
    return outs

def job():
    log.info("parser tag ... start")
    query = "SELECT * "
    query += "FROM crawler_data d "
    query += "left join crawler_tag t on (d.site = t.site and d.id = t.id) "
    query += "Where t.tags is null LIMIT 10"
    exist = "SELECT EXISTS(select * from crawler_tag where site = %s and id = %s)"
    insert = "INSERT INTO crawler_tag (site, id, tags) VALUES(%s, %s, %s)"
    try:
        conn = mysql.connector.connect(
            host="10.20.176.4",
            user="ubuntu",
            password="",
            database="hako"
        )
        cursor = conn.cursor()
        while True:
            cursor.execute(query)
            result = cursor.fetchall()
            log.info("row size:" + str(cursor.rowcount))
            if(cursor.rowcount == 0):
                log.info("parser tag ... done")
                break;
            for row in result:
                cursor.execute(exist, (row[1], row[9]))
                if(cursor.fetchall()[0][0] == 0):
                    log.info("title:" + row[6])
                    intro = json.loads(row[8])
                    pairs = intro.items()
                    sentences = []
                    for key, value in pairs:
                        if row[1] == "youtube":
                            if key == "title" or key == "description":
                                v = value.replace("\n", "")
                                sentences.append(v)
                        else:
                            v = value.replace("\n", "")
                            sentences.append(v)
                    log.info("data:")
                    log.info(sentences)
                    tags = merge(ckip.tags(sentences), inltk.tags(sentences))
                    if row[1] != "youtube":
                        titles = re.split(" |：|~", row[6])
                        for title in titles:
                            if title.startswith("第") and title.endswith("季"):
                                continue
                            tags.append(title)
                    else:
                        if row[4].endswith("發燒影片") == False:
                            tags.append(row[4])
                    js = json.dumps(tags, ensure_ascii=False)
                    log.info("tag:")
                    log.info(js)
                    #update
                    cursor.execute(insert, (row[1], row[9], js))
                    conn.commit()
        cursor.close()
    finally:
        if (conn):
            conn.close()
    log.info("parser tag ... done")


#job()
    
#schedule.every().day.at("04:00").do(job)
schedule.every().hour.do(job)
while True:
    schedule.run_pending()
    time.sleep(1)
    