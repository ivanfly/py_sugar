import logging as log
import nltk
#import ssl
#ssl._create_default_https_context = ssl._create_unverified_context
#nltk.download()
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
FORMAT = '%(asctime)s %(levelname)s: %(message)s'
log.basicConfig(level=log.DEBUG, filename='crawler.log', filemode='w', format=FORMAT)
#sentence_list = ["ivan", "The world needed a hero, we got a hedgehog. Powered with incredible speed, Sonic The Hedgehog (voiced by Ben Schwartz), aka The Blue Blur, embraces his new home on Earth. That is, until he accidentally knocks out the power grid and sparks the attention of super-uncool evil genius Dr. Robotnik (Jim Carrey). Now it’s super-villain vs. super-sonic in an all-out race across the globe to stop Robotnik from using Sonic’s unique power for world domination. Sonic teams up with The Donut Lord, aka Sheriff Tom Wachowski (James Marsden), to save the planet in this live-action adventure comedy that’s fun for the whole family."]
def tags(sentence_list):
    outs = []
    for sentence in sentence_list:
        sents = sent_tokenize(sentence)
        for sent in sents:
            #print(sent)
            words = word_tokenize(sent)
            #print(words)
            tags = nltk.pos_tag(words)
            
            for i in range(0, len(tags), 1):
                tag = tags[i]
                if tag[1].endswith("NNP") and len(tag[0].strip()) > 1 and len(tag[0].strip()) < 20:
                    if tag[0] not in outs:
                        outs.append(tag[0].strip().strip().strip("',.-~!@#$%^&*()-_+=/\\{}[]|"))
            #entities = nltk.chunk.ne_chunk(tagged)
            #for entity in entities:
            #    print(entity)
    log.info("nltk:")
    log.info(outs)
    return outs