import logging as log
FORMAT = '%(asctime)s %(levelname)s: %(message)s'
log.basicConfig(level=log.DEBUG, filename='ckipapp.log', filemode='w', format=FORMAT)

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

from ckiptagger import WS, POS
ws = WS("./data")
pos = POS("./data")

from flask import Flask, request, jsonify, abort

def tags(sentence_list):
    word_sentences_list = ws(
        sentence_list,
        # sentence_segmentation = True, # To consider delimiters
        # segment_delimiter_set = {",", "。", ":", "?", "!", ";"}), # This is the defualt set of delimiters
        # recommend_dictionary = dictionary1, # words in this dictionary are encouraged
        # coerce_dictionary = dictionary2, # words in this dictionary are forced
    )
    pos_sentences_list = pos(word_sentences_list)
    #entity_sentences_list = ner(word_sentences_list, pos_sentences_list)
    #word_sentences = word_sentences_list[0]
    #pos_sentences = pos_sentences_list[0]
    #entity_sentences = entity_sentences_list[0]
    outs = []
    for i in range(0, len(word_sentences_list), 1):
        word_sentences = word_sentences_list[i]
        pos_sentences = pos_sentences_list[i]
        for j in range(0, len(word_sentences), 1):
            w = word_sentences[j].strip().strip("',.-~!@#$%^&*()-_+=/\\{}[]|")
            if(len(w) == 1):
                continue
            #暫時不用Na會太多名詞、Nc以會太多地方詞最多留Nc拿掉Ncd
            if pos_sentences[j] == 'Nb' or pos_sentences[j] == 'Na':
                #print(word_sentences[j] + ":" + pos_sentences[j])
                if w not in outs:
                    outs.append(w)
    #print(entity_sentences_list)
    log.info("ckip:")
    log.info(outs)
    return outs

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello, World!"

@app.route('/nlp', methods=['GET', 'POST'])
def nlp():
    log.info(request.is_json)
    log.info(request.get_json())
    if(request.is_json):
        data = request.get_json()
        return jsonify(tags(data))
    else:
        abort(400)

if __name__ == '__main__':
    #app.run(host='0.0.0.0', debug=True)
    from waitress import serve
    serve(app, host="0.0.0.0", port=5000)