'''
Created on 2020年9月16日

@author: ivan
'''
from sklearn import tree
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn import neighbors
import numpy as np
import matplotlib.pyplot as plt
from tensorboard.plugins.interactive_inference.utils.inference_utils import create_sprite_image

# 讀入鳶尾花資料
iris = load_iris()
iris_X = iris.data
iris_y = iris.target
print(iris_X)
print(iris_y)
# 切分訓練與測試資料
train_X, test_X, train_y, test_y = train_test_split(iris_X, iris_y, test_size = 0.3)

# 建立決策樹分類器
clf = tree.DecisionTreeClassifier()
iris_clf = clf.fit(train_X, train_y)

# 預測
test_y_predicted = iris_clf.predict(test_X)
print(test_y_predicted)

# 標準答案
print(test_y)

# 績效
accuracy = metrics.accuracy_score(test_y, test_y_predicted)
print(accuracy)

# 建立KNN分類器
clf = neighbors.KNeighborsClassifier()
iris_clf = clf.fit(train_X, train_y)

# 預測
test_y_predicted = iris_clf.predict(test_X)
print(test_y_predicted)

# 標準答案
print(test_y)

# 績效
accuracy = metrics.accuracy_score(test_y, test_y_predicted)
print(accuracy)

# 選擇 k
range = np.arange(1, round(0.2 * train_X.shape[0]) + 1)
accuracies = []

for i in range:
    clf = neighbors.KNeighborsClassifier(n_neighbors = i)
    iris_clf = clf.fit(train_X, train_y)
    test_y_predicted = iris_clf.predict(test_X)
    accuracy = metrics.accuracy_score(test_y, test_y_predicted)
    accuracies.append(accuracy)

# 視覺化
plt.scatter(range, accuracies)
plt.show()
appr_k = accuracies.index(max(accuracies)) + 1
print(appr_k)